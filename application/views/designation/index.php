<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Liste Designation</h3>
                <?php if($this->session->userdata('user_type')=='Administrateur' OR $this->session->userdata('user_type')=='RH' ){ ?>
            	<div class="box-tools">
                    <a href="<?php echo site_url('designation/add'); ?>" class="btn btn-success btn-sm">Ajout</a> 
                </div>
                <?php } ?>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>Nom</th>
                        <?php if($this->session->userdata('user_type')=='Administrateur' OR $this->session->userdata('user_type')=='RH' ){ ?>
						<th>Actions</th>
                        <?php } ?>
                    </tr>
                    <?php foreach($designation as $d){ ?>
                    <tr>
						<td><?php echo $d['nom']; ?></td>
                        <?php if($this->session->userdata('user_type')=='Administrateur' OR $this->session->userdata('user_type')=='RH' ){ ?>
						<td>
                            <a href="<?php echo site_url('designation/edit/'.$d['id']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Modifier</a> 
                            <a href="<?php echo site_url('designation/remove/'.$d['id']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Supprimer</a>
                        </td>
                        <?php } ?>
                    </tr>
                    <?php } ?>
                </table>
                <div class="pull-right">
                    <?php echo $this->pagination->create_links(); ?>                    
                </div>                
            </div>
        </div>
    </div>
</div>
