<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Configuration Edit</h3>
            </div>
			<?php echo form_open('configuration/edit/'.$configuration['id']); ?>
			<div class="box-body">
				<div class="row clearfix">
					<div class="col-md-6">
						<label for="logo_site" class="control-label">Logo Site</label>
						<div class="form-group">
							<input type="text" name="logo_site" value="<?php echo ($this->input->post('logo_site') ? $this->input->post('logo_site') : $configuration['logo_site']); ?>" class="form-control" id="logo_site" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="titre_site" class="control-label">Titre Site</label>
						<div class="form-group">
							<input type="text" name="titre_site" value="<?php echo ($this->input->post('titre_site') ? $this->input->post('titre_site') : $configuration['titre_site']); ?>" class="form-control" id="titre_site" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="copyright" class="control-label">Copyright</label>
						<div class="form-group">
							<input type="text" name="copyright" value="<?php echo ($this->input->post('copyright') ? $this->input->post('copyright') : $configuration['copyright']); ?>" class="form-control" id="copyright" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="contact" class="control-label">Contact</label>
						<div class="form-group">
							<input type="text" name="contact" value="<?php echo ($this->input->post('contact') ? $this->input->post('contact') : $configuration['contact']); ?>" class="form-control" id="contact" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="adresse" class="control-label">Adresse</label>
						<div class="form-group">
							<input type="text" name="adresse" value="<?php echo ($this->input->post('adresse') ? $this->input->post('adresse') : $configuration['adresse']); ?>" class="form-control" id="adresse" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="email_systeme" class="control-label">Email Systeme</label>
						<div class="form-group">
							<input type="text" name="email_systeme" value="<?php echo ($this->input->post('email_systeme') ? $this->input->post('email_systeme') : $configuration['email_systeme']); ?>" class="form-control" id="email_systeme" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="description" class="control-label">Description</label>
						<div class="form-group">
							<textarea name="description" class="form-control" id="description"><?php echo ($this->input->post('description') ? $this->input->post('description') : $configuration['description']); ?></textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Save
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>