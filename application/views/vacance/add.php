<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Ajout Vacance</h3>
            </div>
            <?php echo form_open('vacance/add'); ?>
          	<div class="box-body">
          		<div class="row clearfix">
					<div class="col-md-6">
						<label for="nom" class="control-label">Nom</label>
						<div class="form-group">
							<input type="text" name="nom" value="<?php echo $this->input->post('nom'); ?>" class="form-control" id="nom" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="date_debut" class="control-label">Date Debut</label>
						<div class="form-group">
							<input type="text" name="date_debut" value="<?php echo $this->input->post('date_debut'); ?>" class="has-datepicker form-control" id="date_debut" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="date_fin" class="control-label">Date Fin</label>
						<div class="form-group">
							<input type="text" name="date_fin" value="<?php echo $this->input->post('date_fin'); ?>" class="has-datepicker form-control" id="date_fin" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="annee" class="control-label">Annee</label>
						<div class="form-group">
							<input type="text" name="annee" value="<?php echo $this->input->post('annee'); ?>" class="form-control" id="annee" />
						</div>
					</div>
				</div>
			</div>
          	<div class="box-footer">
            	<button type="submit" class="btn btn-success">
            		<i class="fa fa-check"></i> Enregistrer
            	</button>
          	</div>
            <?php echo form_close(); ?>
      	</div>
    </div>
</div>