<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Listes des Taches</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('tache/add'); ?>" class="btn btn-success btn-sm">Ajouter</a> 
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>Projet</th>
						<th>Titre</th>
						<th>Date Debut</th>
						<th>Date Fin</th>
						<th>Statut</th>
						<th>Employe Matricule</th>
						<th>Description</th>
						<th>Actions</th>
                    </tr>
                    <?php foreach($tache as $t){ ?>
                    <tr>
						<td><?php echo $t['projet_id']; ?></td>
						<td><?php echo $t['titre']; ?></td>
						<td><?php echo $t['date_debut']; ?></td>
						<td><?php echo $t['date_fin']; ?></td>
						<td><?php echo $t['statut']; ?></td>
						<td><?php echo $t['employe_matricule']; ?></td>
						<td><?php echo $t['description']; ?></td>
						<td>
                            <a href="<?php echo site_url('tache/edit/'.$t['id']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Modifier</a> 
                            <a href="<?php echo site_url('tache/remove/'.$t['id']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Supprimer</a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                <div class="pull-right">
                    <?php echo $this->pagination->create_links(); ?>                    
                </div>                
            </div>
        </div>
    </div>
</div>
