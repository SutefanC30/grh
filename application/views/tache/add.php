<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Attribuer Tache</h3>
            </div>
            <?php echo form_open('tache/add'); ?>
          	<div class="box-body">
          		<div class="row clearfix">
					<div class="col-md-6">
						<label for="projet_id" class="control-label">Projet</label>
						<div class="form-group">
							<select name="projet_id" class="form-control">
								<option value="">selectionner projet</option>
								<?php 
								foreach($all_projet as $projet)
								{
									$selected = ($projet['id'] == $this->input->post('projet_id')) ? ' selected="selected"' : "";

									echo '<option value="'.$projet['id'].'" '.$selected.'>'.$projet['nom'].'</option>';
								} 
								?>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<label for="titre" class="control-label">Titre</label>
						<div class="form-group">
							<input type="text" name="titre" value="<?php echo $this->input->post('titre'); ?>" class="form-control" id="titre" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="date_debut" class="control-label">Date Debut</label>
						<div class="form-group">
							<input type="text" name="date_debut" value="<?php echo $this->input->post('date_debut'); ?>" class="has-datepicker form-control" id="date_debut" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="date_fin" class="control-label">Date Fin</label>
						<div class="form-group">
							<input type="text" name="date_fin" value="<?php echo $this->input->post('date_fin'); ?>" class="has-datepicker form-control" id="date_fin" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="statut" class="control-label">Statut</label>
						<div class="form-group">
							<select name="statut" class="form-control">
								<option value="">selectionnez statut</option>
								<?php 
								$statuts = ['En cours', 'Terminé', 'Annulé'];
								foreach($statuts as $statut)
								{
									$selected = ($statut == $this->input->post('statut')) ? ' selected="selected"' : "";

									echo '<option value="'.$statut.'" '.$selected.'>'.$statut.'</option>';
								} 
								?>
							</select>
						</div>
					</div>
					<!-- <div class="col-md-6">
						<label for="statut" class="control-label">Statut</label>
						<div class="form-group">
							<input type="text" name="statut" value="<?php echo $this->input->post('statut'); ?>" class="form-control" id="statut" />
						</div>
					</div> -->
					<div class="col-md-6">
						<label for="employe_matricule" class="control-label">Employe Matricule</label>
						<div class="form-group">
							<input type="text" name="employe_matricule" value="<?php echo $this->input->post('employe_matricule'); ?>" class="form-control" id="employe_matricule" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="description" class="control-label">Description</label>
						<div class="form-group">
							<textarea name="description" class="form-control" id="description"><?php echo $this->input->post('description'); ?></textarea>
						</div>
					</div>
				</div>
			</div>
          	<div class="box-footer">
            	<button type="submit" class="btn btn-success">
            		<i class="fa fa-check"></i> Enregistrer
            	</button>
          	</div>
            <?php echo form_close(); ?>
      	</div>
    </div>
</div>