<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Modification Notice</h3>
            </div>
			<?php echo form_open('notice/edit/'.$notice['id']); ?>
			<div class="box-body">
				<div class="row clearfix">
					<div class="col-md-6">
						<label for="titre" class="control-label">Titre</label>
						<div class="form-group">
							<input type="text" name="titre" value="<?php echo ($this->input->post('titre') ? $this->input->post('titre') : $notice['titre']); ?>" class="form-control" id="titre" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="url_fichier" class="control-label">Url Fichier</label>
						<div class="form-group">
							<input type="file" name="url_fichier" value="<?php echo ($this->input->post('url_fichier') ? $this->input->post('url_fichier') : $notice['url_fichier']); ?>" class="form-control" id="url_fichier" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="description" class="control-label">Description</label>
						<div class="form-group">
							<textarea name="description" class="form-control" id="description"><?php echo ($this->input->post('description') ? $this->input->post('description') : $notice['description']); ?></textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Enregistrer
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>