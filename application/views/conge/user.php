<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Vos demandes de congé</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('conge/add'); ?>" class="btn btn-success btn-sm">Ajouter</a> 
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>Type Conge</th>
						<th>Date Debut</th>
						<th>Date Fin</th>
						<th>Duree</th>
						<th>Raison</th>
						<th>Date Application</th>
						<th>Statut</th>
						<th>Actions</th>
                    </tr>
                    <?php foreach($conge as $c){ ?>
                    <tr>
						<td><?php echo $c['type_conge']; ?></td>
						<td><?php echo $c['date_debut']; ?></td>
						<td><?php echo $c['date_fin']; ?></td>
						<td><?php echo $c['duree']; ?></td>
						<td><?php echo $c['raison']; ?></td>
						<td><?php echo $c['date_application']; ?></td>
						<td><?php echo $c['statut']; ?></td>
						<td>
                            <a href="<?php echo site_url('conge/edit/'.$c['id']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Modifier</a> 
                            <a href="<?php echo site_url('conge/remove/'.$c['id']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Supprimer</a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                <div class="pull-right">
                    <?php echo $this->pagination->create_links(); ?>                    
                </div>                
            </div>
        </div>
    </div>
</div>
