<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Modification congé</h3>
            </div>
			<?php echo form_open('conge/edit/'.$conge['id']); ?>
			<div class="box-body">
				<div class="row clearfix">
					<div class="col-md-6">
						<label for="type_conge_id" class="control-label">Type Conge</label>
						<div class="form-group">
							<select name="type_conge_id" class="form-control">
								<option value="">select type_conge</option>
								<?php 
								foreach($all_type_conge as $type_conge)
								{
									$selected = ($type_conge['id'] == $conge['type_conge_id']) ? ' selected="selected"' : "";

									echo '<option value="'.$type_conge['id'].'" '.$selected.'>'.$type_conge['nom'].'</option>';
								} 
								?>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<label for="raison" class="control-label">Raison</label>
						<div class="form-group">
							<input type="text" name="raison" value="<?php echo ($this->input->post('raison') ? $this->input->post('raison') : $conge['raison']); ?>" class="form-control" id="raison" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="date_debut" class="control-label">Date Debut</label>
						<div class="form-group">
							<input type="text" name="date_debut" value="<?php echo ($this->input->post('date_debut') ? $this->input->post('date_debut') : $conge['date_debut']); ?>" class="has-datepicker form-control" id="date_debut" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="date_fin" class="control-label">Date Fin</label>
						<div class="form-group">
							<input type="text" name="date_fin" value="<?php echo ($this->input->post('date_fin') ? $this->input->post('date_fin') : $conge['date_fin']); ?>" class="has-datepicker form-control" id="date_fin" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="duree" class="control-label">Duree</label>
						<div class="form-group">
							<input type="text" name="duree" value="<?php echo ($this->input->post('duree') ? $this->input->post('duree') : $conge['duree']); ?>" class="form-control" id="duree" />
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Enregistrer
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>