<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Penalite Listing</h3>
                <?php if($this->session->userdata('user_type')=='Administrateur' OR $this->session->userdata('user_type')=='RH' ){ ?>
            	<div class="box-tools">
                    <a href="<?php echo site_url('penalite/add'); ?>" class="btn btn-success btn-sm">Ajout</a> 
                </div>
                <?php } ?>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>Nom</th>
                        <?php if($this->session->userdata('user_type')=='Administrateur' OR $this->session->userdata('user_type')=='RH' ){ ?>
						<th>Actions</th>
                        <?php } ?>
                    </tr>
                    <?php foreach($penalite as $p){ ?>
                    <tr>
						<td><?php echo $p['nom']; ?></td>
                        <?php if($this->session->userdata('user_type')=='Administrateur' OR $this->session->userdata('user_type')=='RH' ){ ?>
						<td>
                            <a href="<?php echo site_url('penalite/edit/'.$p['id']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Modifier</a> 
                            <a href="<?php echo site_url('penalite/remove/'.$p['id']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Supprimer</a>
                        </td>
                        <?php } ?>
                    </tr>
                    <?php } ?>
                </table>
                                
            </div>
        </div>
    </div>
</div>
