<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">
                Actualités
                </h3>
            </div>
            <div class="box-body">
                <ul class="timeline">
                <?php foreach($notice as $n){ ?>
                    <li class="time-label">
                        <span class="bg-green">
                            <?php echo $n['date']; ?>
                        </span>
                    </li>
                    <li>
                        <i class="fa fa-clock-o bg-blue"></i>

                        <div class="timeline-item">

                            <h3 class="timeline-header"><a href="#"><?php echo $n['titre']; ?></a></h3>

                            <div class="timeline-body">
                                <?php echo $n['description']; ?>
                                <br>
                                <img src="<?php echo $n['description']; ?>" alt="<?php echo $n['titre']; ?>" class="margin">
                                
                            </div>
                        </div>
                    </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
</div>