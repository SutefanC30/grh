<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Employe prochainement en retraite </h3>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>Matricule</th>
						<th>Photo</th>
						<th>Nom</th>
						<th>Prenom</th>
						<th>Designation</th>
						<th>Departement</th>
						<th>Email</th>
						<th>Date Recrutement</th>
                        <?php if($this->session->userdata('user_type')=='Administrateur' OR $this->session->userdata('user_type')=='RH' ){ ?>
                        <th>Grade</th>
                        <th>Indice</th>
                        <?php } ?>
                        <th>Date Anniversaire</th>
						<th>Actions</th>
                    </tr>
                    <?php foreach($employe as $e){ ?>
                    <tr>
						<td><?php echo $e['matricule']; ?></td>
						<td><img src="<?php echo site_url('resources/img/user2-160x160.jpg');?>" class="img-circle" alt="User Image" height="50px"></td>
						<td><?php echo $e['nom']; ?></td>
						<td><?php echo $e['prenom']; ?></td>
						<td><?php echo $e['designation_id']; ?></td>
						<td><?php echo $e['departement_id']; ?></td>
						<td><?php echo $e['email']; ?></td>
						<td><?php echo $e['date_recrutement']; ?></td>
                        <?php if($this->session->userdata('user_type')=='Administrateur' OR $this->session->userdata('user_type')=='RH' ){ ?>
                        <th><?php echo $e['grade']; ?></th>
                        <th><?php echo $e['indice']; ?></th>
                        <?php } ?>
                        <td><?php echo $e['date_anniv']; ?></td>
						<td>
                            <a href="<?php echo site_url('employe/view/'.$e['matricule']); ?>" class="btn btn-success btn-xs"><span class="fa fa-eye"></span> Voir</a>
                            <?php if($this->session->userdata('user_type')=='Administrateur' OR $this->session->userdata('user_type')=='RH' ){ ?>
                            <a href="<?php echo site_url('employe/edit/'.$e['matricule']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Modifier</a> 
                            <a href="<?php echo site_url('employe/remove/'.$e['matricule']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Supprimer</a>
                            <?php } ?>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                <div class="pull-right">
                    <?php echo $this->pagination->create_links(); ?>                    
                </div>                
            </div>
        </div>
    </div>
</div>
