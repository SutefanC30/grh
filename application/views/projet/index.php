<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Projet Listing</h3>
                <?php if($this->session->userdata('user_type')=='Administrateur' OR $this->session->userdata('user_type')=='RH' ){ ?>
            	<div class="box-tools">
                    <a href="<?php echo site_url('projet/add'); ?>" class="btn btn-success btn-sm">Ajout</a> 
                </div>
                <?php } ?>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>Nom</th>
						<th>Date Debut</th>
						<th>Date Fin</th>
						<th>Statut</th>
						<th>Description</th>
						<th>Resume</th>
                        <?php if($this->session->userdata('user_type')=='Administrateur' OR $this->session->userdata('user_type')=='RH' ){ ?>
						<th>Actions</th>
                        <?php } ?>
                    </tr>
                    <?php foreach($projet as $p){ ?>
                    <tr>
						<td><?php echo $p['nom']; ?></td>
						<td><?php echo $p['date_debut']; ?></td>
						<td><?php echo $p['date_fin']; ?></td>
						<td><?php echo $p['statut']; ?></td>
						<td><?php echo $p['description']; ?></td>
						<td><?php echo $p['resume']; ?></td>
                        <?php if($this->session->userdata('user_type')=='Administrateur' OR $this->session->userdata('user_type')=='RH' ){ ?>
						<td>
                            <a href="<?php echo site_url('projet/edit/'.$p['id']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Modifier</a> 
                            <a href="<?php echo site_url('projet/remove/'.$p['id']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Supprimer</a>
                        </td>
                        <?php } ?>
                    </tr>
                    <?php } ?>
                </table>
                <div class="pull-right">
                    <?php echo $this->pagination->create_links(); ?>                    
                </div>                
            </div>
        </div>
    </div>
</div>
