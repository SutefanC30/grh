<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Ajout Type Congé</h3>
            </div>
            <?php echo form_open('type_conge/add'); ?>
          	<div class="box-body">
          		<div class="row clearfix">
					<div class="col-md-6">
						<label for="nom" class="control-label">Nom</label>
						<div class="form-group">
							<input type="text" name="nom" value="<?php echo $this->input->post('nom'); ?>" class="form-control" id="nom" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="nb_jour" class="control-label">Nb Jour</label>
						<div class="form-group">
							<input type="text" name="nb_jour" value="<?php echo $this->input->post('nb_jour'); ?>" class="form-control" id="nb_jour" />
						</div>
					</div>
				</div>
			</div>
          	<div class="box-footer">
            	<button type="submit" class="btn btn-success">
            		<i class="fa fa-check"></i> Enregistrer
            	</button>
          	</div>
            <?php echo form_close(); ?>
      	</div>
    </div>
</div>