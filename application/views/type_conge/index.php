<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Types Congé</h3>
                <?php if($this->session->userdata('user_type')=='Administrateur' OR $this->session->userdata('user_type')=='RH' ){ ?>
            	<div class="box-tools">
                    <a href="<?php echo site_url('type_conge/add'); ?>" class="btn btn-success btn-sm">Ajout</a> 
                </div>
                <?php } ?>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>Nom</th>
						<th>Nb Jour</th>
                        <?php if($this->session->userdata('user_type')=='Administrateur' OR $this->session->userdata('user_type')=='RH' ){ ?>
						<th>Actions</th>
                        <?php } ?>
                    </tr>
                    <?php foreach($type_conge as $t){ ?>
                    <tr>
						<td><?php echo $t['nom']; ?></td>
						<td><?php echo $t['nb_jour']; ?></td>
                        <?php if($this->session->userdata('user_type')=='Administrateur' OR $this->session->userdata('user_type')=='RH' ){ ?>
						<td>
                            <a href="<?php echo site_url('type_conge/edit/'.$t['id']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Modifier</a> 
                            <a href="<?php echo site_url('type_conge/remove/'.$t['id']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Supprimer</a>
                        </td>
                        <?php } ?>
                    </tr>
                    <?php } ?>
                </table>
                                
            </div>
        </div>
    </div>
</div>
