<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Modification Penalité Employé</h3>
            </div>
			<?php echo form_open('penalite_employe/edit/'.$penalite_employe['id']); ?>
			<div class="box-body">
				<div class="row clearfix">
					<div class="col-md-6">
						<label for="penalite_id" class="control-label">Penalité</label>
						<div class="form-group">
							<select name="penalite_id" class="form-control">
								<option value="">selectionnez penalite</option>
								<?php 
								foreach($all_penalite as $penalite)
								{
									$selected = ($penalite['id'] == $penalite_employe['penalite_id']) ? ' selected="selected"' : "";

									echo '<option value="'.$penalite['id'].'" '.$selected.'>'.$penalite['nom'].'</option>';
								} 
								?>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<label for="employe_matricule" class="control-label">Employé</label>
						<div class="form-group">
							<input type="text" name="employe_matricule" value="<?php echo ($this->input->post('employe_matricule') ? $this->input->post('employe_matricule') : $penalite_employe['employe_matricule']); ?>" class="form-control" id="employe_matricule" />
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Enregistrer
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>