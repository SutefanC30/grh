<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Liste Penalité Employé</h3>
                <?php if($this->session->userdata('user_type')=='Administrateur' OR $this->session->userdata('user_type')=='RH' ){ ?>
            	<div class="box-tools">
                    <a href="<?php echo site_url('penalite_employe/add'); ?>" class="btn btn-success btn-sm">Ajout</a> 
                </div>
                <?php } ?>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>Penalité</th>
						<th>Employe Matricule</th>
                        <?php if($this->session->userdata('user_type')=='Administrateur' OR $this->session->userdata('user_type')=='RH' ){ ?>
						<th>Actions</th>
                        <?php } ?>
                    </tr>
                    <?php foreach($penalite_employe as $p){ ?>
                    <tr>
						<td><?php echo $p['id']; ?></td>
						<td><?php echo $p['penalite_id']; ?></td>
						<td><?php echo $p['employe_matricule']; ?></td>
                        <?php if($this->session->userdata('user_type')=='Administrateur' OR $this->session->userdata('user_type')=='RH' ){ ?>
						<td>
                            <a href="<?php echo site_url('penalite_employe/edit/'.$p['id']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Modifier</a> 
                            <a href="<?php echo site_url('penalite_employe/remove/'.$p['id']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Supprimer</a>
                        </td>
                        <?php } ?>
                    </tr>
                    <?php } ?>
                </table>
                <div class="pull-right">
                    <?php echo $this->pagination->create_links(); ?>                    
                </div>                
            </div>
        </div>
    </div>
</div>
