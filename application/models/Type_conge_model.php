<?php
 
class Type_conge_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get type_conge by id
     */
    function get_type_conge($id)
    {
        return $this->db->get_where('type_conge',array('id'=>$id))->row_array();
    }
        
    /*
     * Get all type_conge
     */
    function get_all_type_conge()
    {
        $this->db->order_by('id', 'desc');
        return $this->db->get('type_conge')->result_array();
    }
        
    /*
     * function to add new type_conge
     */
    function add_type_conge($params)
    {
        $this->db->insert('type_conge',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update type_conge
     */
    function update_type_conge($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('type_conge',$params);
    }
    
    /*
     * function to delete type_conge
     */
    function delete_type_conge($id)
    {
        return $this->db->delete('type_conge',array('id'=>$id));
    }
}
