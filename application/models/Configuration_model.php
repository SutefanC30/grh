<?php

 
class Configuration_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get configuration by id
     */
    function get_configuration($id)
    {
        return $this->db->get_where('configuration',array('id'=>$id))->row_array();
    }
        
    /*
     * Get all configuration
     */
    function get_all_configuration()
    {
        $this->db->order_by('id', 'desc');
        return $this->db->get('configuration')->result_array();
    }
        
    /*
     * function to add new configuration
     */
    function add_configuration($params)
    {
        $this->db->insert('configuration',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update configuration
     */
    function update_configuration($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('configuration',$params);
    }
    
    /*
     * function to delete configuration
     */
    function delete_configuration($id)
    {
        return $this->db->delete('configuration',array('id'=>$id));
    }
}
