<?php
 
class Vacance_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get vacance by id
     */
    function get_vacance($id)
    {
        return $this->db->get_where('vacance',array('id'=>$id))->row_array();
    }
        
    /*
     * Get all vacance
     */
    function get_all_vacance()
    {
        $this->db->order_by('id', 'desc');
        return $this->db->get('vacance')->result_array();
    }
        
    /*
     * function to add new vacance
     */
    function add_vacance($params)
    {
        $this->db->insert('vacance',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update vacance
     */
    function update_vacance($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('vacance',$params);
    }
    
    /*
     * function to delete vacance
     */
    function delete_vacance($id)
    {
        return $this->db->delete('vacance',array('id'=>$id));
    }
}
