<?php
 
class Tache_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get tache by id
     */
    function get_tache($id)
    {
        return $this->db->get_where('tache',array('id'=>$id))->row_array();
    }
    
    /*
     * Get all tache count
     */
    function get_all_tache_count()
    {
        $this->db->from('tache');
        return $this->db->count_all_results();
    }
        
    /*
     * Get all tache
     */
    function get_all_tache($params = array())
    {
        $this->db->order_by('id', 'desc');
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        return $this->db->get('tache')->result_array();
    }
        
    /*
     * function to add new tache
     */
    function add_tache($params)
    {
        $this->db->insert('tache',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update tache
     */
    function update_tache($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('tache',$params);
    }
    
    /*
     * function to delete tache
     */
    function delete_tache($id)
    {
        return $this->db->delete('tache',array('id'=>$id));
    }
}
