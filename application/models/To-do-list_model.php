<?php
 
class To-do-list_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get to-do-list by id
     */
    function get_to-do-list($id)
    {
        return $this->db->get_where('to-do-list',array('id'=>$id))->row_array();
    }
        
    /*
     * Get all to-do-list
     */
    function get_all_to-do-list()
    {
        $this->db->order_by('id', 'desc');
        return $this->db->get('to-do-list')->result_array();
    }
        
    /*
     * function to add new to-do-list
     */
    function add_to-do-list($params)
    {
        $this->db->insert('to-do-list',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update to-do-list
     */
    function update_to-do-list($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('to-do-list',$params);
    }
    
    /*
     * function to delete to-do-list
     */
    function delete_to-do-list($id)
    {
        return $this->db->delete('to-do-list',array('id'=>$id));
    }
}
