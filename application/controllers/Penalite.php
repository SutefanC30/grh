<?php

class Penalite extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Penalite_model');
    } 

    /*
     * Listing of penalite
     */
    function index()
    {
        $data['penalite'] = $this->Penalite_model->get_all_penalite();
        
        $data['_view'] = 'penalite/index';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Adding a new penalite
     */
    function add()
    {   
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
				'nom' => $this->input->post('nom'),
            );
            
            $penalite_id = $this->Penalite_model->add_penalite($params);
            redirect('penalite/index');
        }
        else
        {            
            $data['_view'] = 'penalite/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a penalite
     */
    function edit($id)
    {   
        // check if the penalite exists before trying to edit it
        $data['penalite'] = $this->Penalite_model->get_penalite($id);
        
        if(isset($data['penalite']['id']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
					'nom' => $this->input->post('nom'),
                );

                $this->Penalite_model->update_penalite($id,$params);            
                redirect('penalite/index');
            }
            else
            {
                $data['_view'] = 'penalite/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The penalite you are trying to edit does not exist.');
    } 

    /*
     * Deleting penalite
     */
    function remove($id)
    {
        $penalite = $this->Penalite_model->get_penalite($id);

        // check if the penalite exists before trying to delete it
        if(isset($penalite['id']))
        {
            $this->Penalite_model->delete_penalite($id);
            redirect('penalite/index');
        }
        else
            show_error('The penalite you are trying to delete does not exist.');
    }
    
}
