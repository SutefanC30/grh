<?php

 
class Penalite_employe extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Penalite_employe_model');
    } 

    /*
     * Listing of penalite_employe
     */
    function index()
    {
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('penalite_employe/index?');
        $config['total_rows'] = $this->Penalite_employe_model->get_all_penalite_employe_count();
        $this->pagination->initialize($config);

        $data['penalite_employe'] = $this->Penalite_employe_model->get_all_penalite_employe($params);
        
        $data['_view'] = 'penalite_employe/index';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Adding a new penalite_employe
     */
    function add()
    {   
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
				'penalite_id' => $this->input->post('penalite_id'),
				'employe_matricule' => $this->input->post('employe_matricule'),
            );
            
            $penalite_employe_id = $this->Penalite_employe_model->add_penalite_employe($params);
            redirect('penalite_employe/index');
        }
        else
        {
			$this->load->model('Penalite_model');
			$data['all_penalite'] = $this->Penalite_model->get_all_penalite();
            
            $data['_view'] = 'penalite_employe/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a penalite_employe
     */
    function edit($id)
    {   
        // check if the penalite_employe exists before trying to edit it
        $data['penalite_employe'] = $this->Penalite_employe_model->get_penalite_employe($id);
        
        if(isset($data['penalite_employe']['id']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
					'penalite_id' => $this->input->post('penalite_id'),
					'employe_matricule' => $this->input->post('employe_matricule'),
                );

                $this->Penalite_employe_model->update_penalite_employe($id,$params);            
                redirect('penalite_employe/index');
            }
            else
            {
				$this->load->model('Penalite_model');
				$data['all_penalite'] = $this->Penalite_model->get_all_penalite();

                $data['_view'] = 'penalite_employe/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The penalite_employe you are trying to edit does not exist.');
    } 

    /*
     * Deleting penalite_employe
     */
    function remove($id)
    {
        $penalite_employe = $this->Penalite_employe_model->get_penalite_employe($id);

        // check if the penalite_employe exists before trying to delete it
        if(isset($penalite_employe['id']))
        {
            $this->Penalite_employe_model->delete_penalite_employe($id);
            redirect('penalite_employe/index');
        }
        else
            show_error('The penalite_employe you are trying to delete does not exist.');
    }
    
}
