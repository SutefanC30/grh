<?php

 
class Employe extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Employe_model');
    } 

    /*
     * Listing of employe
     */
    function index()
    {
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('employe/index?');
        $config['total_rows'] = $this->Employe_model->get_all_employe_count();
        $this->pagination->initialize($config);

        $data['employe'] = $this->Employe_model->get_all_employe($params);
        
        $data['_view'] = 'employe/index';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Listing of employe to retired
     */
    function proche_retraite()
    {
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('employe/proche_retraite?');
        $config['total_rows'] = $this->Employe_model->get_all_employe_count();
        $this->pagination->initialize($config);

        $data['employe'] = $this->Employe_model->get_to_retired($params);
        
        $data['_view'] = 'employe/proche_retraite';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Adding a new employe
     */
    function add()
    {   
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
                'matricule' => $this->input->post('matricule'),
				'designation_id' => $this->input->post('designation_id'),
				'departement_id' => $this->input->post('departement_id'),
				'nom' => $this->input->post('nom'),
				'prenom' => $this->input->post('prenom'),
				'email' => $this->input->post('email'),
				'mot_de_passe' => $this->input->post('mot_de_passe'),
				'role' => $this->input->post('role'),
				'adresse' => $this->input->post('adresse'),
				'statut' => $this->input->post('statut'),
				'genre' => $this->input->post('genre'),
				'tel' => $this->input->post('tel'),
				'date_anniv' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('date_anniv')))),
				'date_recrutement' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('date_recrutement')))),
				'date_fin_contrat' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('date_fin_contrat')))),
				'photo' => $this->input->post('photo'),
                'conge_restant' => 28,
                'indice' => $this->input->post('indice'),
                'grade' => $this->input->post('grade'),
            );
            
            $employe_id = $this->Employe_model->add_employe($params);
            redirect('employe/index');
        }
        else
        {
			$this->load->model('Designation_model');
			$data['all_designation'] = $this->Designation_model->get_all_designation();

			$this->load->model('Departement_model');
			$data['all_departement'] = $this->Departement_model->get_all_departement();
            
            $data['_view'] = 'employe/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a employe
     */
    function edit($matricule)
    {   
        // check if the employe exists before trying to edit it
        $data['employe'] = $this->Employe_model->get_employe($matricule);
        
        if(isset($data['employe']['matricule']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
					'designation_id' => $this->input->post('designation_id'),
					'departement_id' => $this->input->post('departement_id'),
					'nom' => $this->input->post('nom'),
					'prenom' => $this->input->post('prenom'),
					'email' => $this->input->post('email'),
					'mot_de_passe' => $this->input->post('mot_de_passe'),
					'role' => $this->input->post('role'),
					'adresse' => $this->input->post('adresse'),
					'statut' => $this->input->post('statut'),
					'genre' => $this->input->post('genre'),
					'tel' => $this->input->post('tel'),
					'date_anniv' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('date_anniv')))),
                    'date_recrutement' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('date_recrutement')))),
                    'date_fin_contrat' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('date_fin_contrat')))),
					'photo' => $this->input->post('photo'),
                    'conge_restant' => $this->input->post('conge_restant'),
                    'indice' => $this->input->post('indice'),
                    'grade' => $this->input->post('grade'),
                    
                );

                $this->Employe_model->update_employe($matricule,$params);            
                redirect('employe/index');
            }
            else
            {
				$this->load->model('Designation_model');
				$data['all_designation'] = $this->Designation_model->get_all_designation();

				$this->load->model('Departement_model');
				$data['all_departement'] = $this->Departement_model->get_all_departement();

                $data['_view'] = 'employe/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The employe you are trying to edit does not exist.');
    } 

    /*
     * Deleting employe
     */
    function remove($matricule)
    {
        $employe = $this->Employe_model->get_employe($matricule);

        // check if the employe exists before trying to delete it
        if(isset($employe['matricule']))
        {
            $this->Employe_model->delete_employe($matricule);
            redirect('employe/index');
        }
        else
            show_error('The employe you are trying to delete does not exist.');
    }
    
}
