<?php

class Dashboard extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Notice_model');
    } 

    function index()
    {
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('dashboard?');
        $config['total_rows'] = $this->Notice_model->get_all_notice_count();
        $this->pagination->initialize($config);

        $data['notice'] = $this->Notice_model->get_all_notice($params);
        
        $data['_view'] = 'dashboard';
        $this->load->view('layouts/main',$data);
    }
}
