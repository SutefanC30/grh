<?php

 
class Tache extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Tache_model');
    } 

    /*
     * Listing of tache
     */
    function index()
    {
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('tache/index?');
        $config['total_rows'] = $this->Tache_model->get_all_tache_count();
        $this->pagination->initialize($config);

        $data['tache'] = $this->Tache_model->get_all_tache($params);
        
        $data['_view'] = 'tache/index';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Adding a new tache
     */
    function add()
    {   
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
				'projet_id' => $this->input->post('projet_id'),
				'titre' => $this->input->post('titre'),
				'date_debut' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('date_debut')))),
				'date_fin' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('date_fin')))),
				'statut' => $this->input->post('statut'),
				'employe_matricule' => $this->input->post('employe_matricule'),
				'description' => $this->input->post('description'),
            );
            
            $tache_id = $this->Tache_model->add_tache($params);
            redirect('tache/index');
        }
        else
        {
			$this->load->model('Projet_model');
			$data['all_projet'] = $this->Projet_model->get_all_projet();
            
            $data['_view'] = 'tache/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a tache
     */
    function edit($id)
    {   
        // check if the tache exists before trying to edit it
        $data['tache'] = $this->Tache_model->get_tache($id);
        
        if(isset($data['tache']['id']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
					'projet_id' => $this->input->post('projet_id'),
					'titre' => $this->input->post('titre'),
					'date_debut' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('date_debut')))),
					'date_fin' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('date_fin')))),
					'statut' => $this->input->post('statut'),
					'employe_matricule' => $this->input->post('employe_matricule'),
					'description' => $this->input->post('description'),
                );

                $this->Tache_model->update_tache($id,$params);            
                redirect('tache/index');
            }
            else
            {
				$this->load->model('Projet_model');
				$data['all_projet'] = $this->Projet_model->get_all_projet();

                $data['_view'] = 'tache/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The tache you are trying to edit does not exist.');
    } 

    /*
     * Deleting tache
     */
    function remove($id)
    {
        $tache = $this->Tache_model->get_tache($id);

        // check if the tache exists before trying to delete it
        if(isset($tache['id']))
        {
            $this->Tache_model->delete_tache($id);
            redirect('tache/index');
        }
        else
            show_error('The tache you are trying to delete does not exist.');
    }
    
}
