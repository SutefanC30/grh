<?php
 
class Configuration extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Configuration_model');
    } 

    /*
     * Listing of configuration
     */
    function index()
    {
        $data['configuration'] = $this->Configuration_model->get_all_configuration();
        
        $data['_view'] = 'configuration/index';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Editing a configuration
     */
    function edit($id)
    {   
        // check if the configuration exists before trying to edit it
        $data['configuration'] = $this->Configuration_model->get_configuration($id);
        
        if(isset($data['configuration']['id']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
					'logo_site' => $this->input->post('logo_site'),
					'titre_site' => $this->input->post('titre_site'),
					'copyright' => $this->input->post('copyright'),
					'contact' => $this->input->post('contact'),
					'adresse' => $this->input->post('adresse'),
					'email_systeme' => $this->input->post('email_systeme'),
					'description' => $this->input->post('description'),
                );

                $this->Configuration_model->update_configuration($id,$params);            
                redirect('configuration/index');
            }
            else
            {
                $data['_view'] = 'configuration/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The configuration you are trying to edit does not exist.');
    } 
    
}
